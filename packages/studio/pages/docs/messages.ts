import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  title: {
    id: 'studio.isGKnz',
    defaultMessage: 'Documentation',
  },
  search: {
    id: 'studio.xmcVZ0',
    defaultMessage: 'Search',
  },
  reference: {
    id: 'studio.mcUhhf',
    defaultMessage: 'Reference',
  },
  app: {
    id: 'studio.2rUVsU',
    defaultMessage: 'App',
  },
  action: {
    id: 'studio.QlsDcr',
    defaultMessage: 'Action',
  },
  changelog: {
    id: 'studio.MQcAql',
    defaultMessage: 'Changelog',
  },
  remapper: {
    id: 'studio.UhFN32',
    defaultMessage: 'Remapper',
  },
  packages: {
    id: 'studio.AZSxPR',
    defaultMessage: 'Packages',
  },
});
